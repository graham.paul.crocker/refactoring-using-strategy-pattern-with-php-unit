<?php

namespace App;

use App\Renderer\RendererInterface;

class Renderer
{
    /**
     * @var RendererInterface[]
     */
    private $renderers;

    /**
     * Renderer constructor.
     * @param array $renderers
     */
    public function __construct(array $renderers)
    {
        $this->renderers = $renderers;
        if (empty($this->renderers['default'])) {
            throw new \InvalidArgumentException('Default renderer should be provided');
        }
    }

    public function render(string $data = '', string $format = null) : string
    {
        if (array_key_exists($format, $this->renderers)) {
            return $this->renderers[$format]->render($data);
        }
        return $this->renderers['default']->render($data);
    }

}