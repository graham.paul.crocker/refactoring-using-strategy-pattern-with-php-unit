<?php

namespace App\Renderer;

interface RendererInterface
{
    public function render(string $data = '') : string;
}