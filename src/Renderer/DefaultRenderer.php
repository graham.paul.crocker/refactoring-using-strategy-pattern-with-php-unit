<?php

namespace App\Renderer;

class DefaultRenderer implements RendererInterface
{

    public function render(string $data = ''): string
    {
        return $data;
    }
}