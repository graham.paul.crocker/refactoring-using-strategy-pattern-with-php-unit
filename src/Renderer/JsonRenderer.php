<?php

namespace App\Renderer;

use App\Renderer;

class JsonRenderer implements RendererInterface
{
    public function render(string $data = ''): string
    {
        if (!$data) {
            return '';
        }
        return json_encode($data);
    }
}