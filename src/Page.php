<?php

namespace App;

class Page
{
    const FORMAT_TYPE_XML = 'xml';
    const FORMAT_TYPE_JSON = 'json';
    const TYPE_DEFAULT = 'text';

    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var string
     */
    private $text;

    /**
     * Page constructor.
     * @param $renderer
     * @param $text
     */
    public function __construct($renderer, $text)
    {
        $this->renderer = $renderer;
        $this->text = $text;
    }

    public function render(string $format = '') : string
    {
        return $this->renderer->render($this->text, $format);
    }

    public function setText($text) : void
    {
        $this->text = $text;
    }

    public function getText() : string
    {
        return $this->text;
    }
}