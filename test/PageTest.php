<?php

declare(strict_types=1);

namespace Test;

use App\Page;
use App\Renderer;
use App\Renderer\DefaultRenderer;
use PHPUnit\Framework\TestCase;

class PageTest extends TestCase
{
    protected $page;

    private $renderer;

    protected function setUp() : void
    {
        $this->renderer = new Renderer([
            'default' => new DefaultRenderer(),
            'json' => new Renderer\JsonRenderer(),
            'xml' => new Renderer\XmlRenderer()
        ]);
        $this->page = new Page( $this->renderer, 'Hello');
    }
    public function testInstanceOf(): void
    {
        $this->assertInstanceOf(Page::class, $this->page);
    }

    public function testRender(): void
    {
        $this->assertEquals('Hello', $this->page->render());
    }

    public function testRenderJson() : void
    {
        $this->assertJson($this->page->render(Page::FORMAT_TYPE_JSON));
    }

    public function testRenderXml() : void
    {
        $document = new \DOMDocument();
        $root = $document->createElement('page');
        $document->appendChild($root);
        $node = $document->createElement('text');
        $node->nodeValue = $this->page->getText();
        $root->appendChild($node);
        $expectedXmlString = $document->saveXml();
        $this->assertEquals($expectedXmlString, $this->page->render(Page::FORMAT_TYPE_XML));
    }


    public function testRenderAnyText() : void
    {
        $expected = 'Hello World';
        $this->page->setText($expected);
        $this->assertEquals($expected, $this->page->render());
    }
}