<?php
namespace Test\Renderer;

use App\Page;
use App\Renderer\JsonRenderer;
use App\Renderer\RendererInterface;

class JsonRendererTest extends \PHPUnit\Framework\TestCase
{
    private $jsonRenderer;

    public function setUp(): void
    {
        $this->jsonRenderer = new JsonRenderer();
        parent::setUp();
    }

    public function testClassExists() : void
    {
        $this->assertInstanceOf(JsonRenderer::class, $this->jsonRenderer);
    }

    public function testRenderMethod() : void
    {
        $this->assertEmpty($this->jsonRenderer->render());
    }

    public function testJsonOutput()   : void
    {
        $this->assertJson($this->jsonRenderer->render('test'));
    }

    public function testInterfaceExists(): void
    {
        $this->assertInstanceOf(RendererInterface::class, $this->jsonRenderer);
    }
}