<?php

namespace Test\Renderer;

use App\Renderer\RendererInterface;
use PHPUnit\Framework\TestCase;
use App\Renderer\DefaultRenderer;

class DefaultRendererTest extends TestCase
{
    private $defaultRenderer;

    public function setUp(): void
    {
        $this->defaultRenderer = new DefaultRenderer();
        parent::setUp();
    }

    public function testClassExists() : void
    {
        $this->assertInstanceOf(DefaultRenderer::class, $this->defaultRenderer);
    }

    public function testInterfaceExists() : void
    {
        $this->assertInstanceOf(RendererInterface::class, $this->defaultRenderer);
    }

    public function testRenderReturnsSameValue() : void
    {
        $this->assertEquals('test', $this->defaultRenderer->render('test'));
    }
}