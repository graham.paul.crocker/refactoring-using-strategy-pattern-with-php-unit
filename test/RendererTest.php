<?php


namespace Test;

use PHPUnit\Framework\TestCase;
use App\Renderer;
use App\Renderer\DefaultRenderer;

class RendererTest extends TestCase
{
    private $renderer;

    public function setUp() : void
    {
        $this->renderer = new Renderer([
            'default' => new DefaultRenderer(),
            'json' => new Renderer\JsonRenderer()
        ]);
    }

    public function testClassExists(): void
    {
        $this->assertInstanceOf(Renderer::class, $this->renderer);
    }

    public function testRenderMethodExists(): void
    {
        $this->assertEmpty($this->renderer->render());
    }

    public function testRenderMethodReturnValue() : void
    {
        $expected = 'string value';
        $this->assertNotEmpty($expected, $this->renderer->render($expected));
    }

    public function testDefaultRendererSetInConstructException() : void
    {
        $this->expectException(\InvalidArgumentException::class);
        new Renderer([]);
    }

    public function testRendererAcceptsFormat() : void
    {
        $format = 'json';
        $data = 'hello';
        $this->assertJson($this->renderer->render($data, $format));
    }


}